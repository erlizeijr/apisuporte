﻿////using glib;
////using system;
////using system.collections.generic;
////using system.linq;
////using system.net.sockets;
////using system.text;
////using system.threading.tasks;
////using visioforge.mediaframework.onvif;
////using system.threading;
////using thread = glib.thread;
//using Advantech.Adam;
//using Advantech.Common;
//using Advantech.Protocol;
//using ModbusTCP;
//using System.Net.Sockets;
//using System.Threading;
//using System.Threading.Tasks;

//namespace apisuporte.models
//{
//    public class adam
//    {
//        private static readonly int totaldigitalinputchannels = DigitalInput.GetChannelTotal(Adam6000Type.Adam6052);
//        private static readonly int totaldigitaloutputchannels = DigitalOutput.GetChannelTotal(Adam6000Type.Adam6052);
//        private const int idsartforinputchanel = 1;
//        private const int idsartforoutputchannel = 17;
//        private bool m_bstart;
//        private int m_idototal, m_iditotal;
//        private int m_iport;
//        private string m_szip;
//        private AdamSocket adammodbus;

//        private static readonly int totaldigitalimputchannels = DigitalInput.GetChannelTotal(Adam6000Type.Adam6052);
//        public void initcontrol(string ipadam, int port)
//        {
//            m_bstart = false;  // the action stops at the beginning
//            m_szip = ipadam;   // modbus slave ip address
//            m_iport = port;               // modbus tcp port is 502
//            adammodbus = new AdamSocket();
//            adammodbus.SetTimeout(1000, 1000, 1000); // set timeout for tcp
//            m_idototal = 7;
//            m_iditotal = 7;
//        }

//        public bool writeoutputchannel(int channel, bool val)
//        {
//            var res = false;

//            if (adammodbus.Connect(m_szip, ProtocolType.Tcp, m_iport))
//            {
//                var modbus = adammodbus.Modbus();
//                res = modbus.ForceSingleCoil(idsartforoutputchannel + channel, val);
//                adammodbus.Disconnect();
//                return res;
//            }
//            else
//            {
//                adammodbus.Disconnect();
//            }
//            return res;
//        }

//        public bool writeoutputchannelwithcheck(string ipadam, int port, int channel, bool val)
//        {
//            ipadam = ipadam.Replace(" ", "");
//            initcontrol(ipadam, port);

//            var res = false;
//            bool[] digitaloutputchannels = null;
//            if (adammodbus.Connect(m_szip, ProtocolType.Tcp, m_iport))
//            {
//                var modbus = adammodbus.Modbus();
//                res = modbus.ForceSingleCoil(idsartforoutputchannel + channel, val);
//                await Task.Delay(5000); //pause de 2 segundos
//                Thread.Sleep(1500);
//                digitaloutputchannels = null;
//                digitaloutputchannels = readoutputchannels();

//                if (digitaloutputchannels[channel] == true)
//                {
//                    //messagebox.show("si la alarma cambio para desactivado desconsidere está mensaje." +
//                    //    "pero si la alrma no cambio para desactivado, tuve algun error en la conexion con el equipo adam. " +
//                    //    "espere de 30 segundo y vuelva a intentarlo!!" +
//                    //    "(obs porta do adam não mudou para false)", "error", messageboxbuttons.ok, messageboxicon.error);
//                    adammodbus.Disconnect();
//                    res = false;
//                    return res;
//                }

//                adammodbus.Disconnect();
//                return res;
//            }
//            else
//            {
//                adammodbus.Disconnect();
//                //messagebox.show("error al escribir en la output adam. (provavelmente error de conexao com adam.)", "error", messageboxbuttons.ok, messageboxicon.error);
//            }
//            return res;
//        }

//        public bool[] readoutputchannels()
//        {
//            bool[] digitaloutputchannels = null;
//            if (adammodbus.Connect(m_szip, ProtocolType.Tcp, m_iport))
//            {
//                var modbus = adammodbus.Modbus();
//                modbus.ReadCoilStatus(idsartforoutputchannel, totaldigitaloutputchannels, out digitaloutputchannels);
//                adammodbus.Disconnect();
//                return digitaloutputchannels;
//            }
//            else
//            {
//                adammodbus.Disconnect();
//                //messagebox.show("error de lectura de output adam", "error", messageboxbuttons.ok, messageboxicon.error);
//            }
//            return digitaloutputchannels;
//        }
//        public bool[] readinputchannels()
//        {
//            bool[] digitalinputchannels = null;

//            if (adammodbus.Connect(m_szip, ProtocolType.Tcp, m_iport))
//            {
//                var modbus = adammodbus.Modbus();
//                modbus.ReadCoilStatus(idsartforinputchanel, totaldigitalinputchannels, out digitalinputchannels);
//                adammodbus.Disconnect();
//                return digitalinputchannels;
//            }
//            else
//            {
//                adammodbus.Disconnect();
//                //messagebox.show("error de lectura de input adam", "error", messageboxbuttons.ok, messageboxicon.error);
//            }
//            return digitalinputchannels;
//        }
//    }
//}
