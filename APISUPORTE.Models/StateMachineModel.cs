﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APISUPORTE.Models
{
    public class StateMachineModel
    {

        public int IdStateMachine { get; set; }
        public string NomeStateMachine { get; set; }
        public int AutoStart { get; set; }
        

    }
    public class State
    {
        public StateMachineModel Estacao { get; set; }

    }

    public class States
    {
        public List<State> Estacaos { get; set; }
    }
}
