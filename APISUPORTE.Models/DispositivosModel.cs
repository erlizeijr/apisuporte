﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APISUPORTE.Models
{
    class DispositivosModel
    {

        public string Dispositivo { get; set; }
        public int DispositivoStatus { get; set; }
        public string CodDispositivo { get; set; }


    }
}
