﻿using APISUPORTE.Service.Interfaces;
using Intercom.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APISUPORTE.Models;
using System.Data.SqlClient;
using APISUPORTE.Service.DB;
using System.Data;
using APISUPORTE.Util;

namespace APISUPORTE.Service
{
    public class ProcessoService : IProcessoService
    {
        
        public async Task<Models.Processos> GetProcesso (int id, string motorista,string banco)
        {
            SqlCommand cmd = new SqlCommand();
            Connection cn = new Connection();
            try
            {

                string newId = id.ToString();
                if (id == 0)
                {
                    newId = "0";

                }
                ////string newMotorista = motorista;
                //if (motorista == null || motorista == "")
                //{
                //    newMotorista = "null";
                //}
                string newBanco = banco;
                if (String.IsNullOrEmpty(banco))
                {
                    newBanco = "null";
                }

                string sqlQuery = " EXEC Processo_Select  " + newId + ",'" + newBanco + "'";
                //string sqlQuery = "EXEC Stpprv_AplPROCESSO_Selecionar " + newId + ",null,null,null,null,null,null,null,null" +
                //",null,null,null,null,null,null,null,null";
                cmd.CommandText = sqlQuery;
                cmd.Connection = cn.Conectar(banco);
                cmd.ExecuteReader();
                cn.Desconectar();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable TabelaProcesso = new DataTable();
                da.Fill(TabelaProcesso);

                var listMovimento = new List<Models.Processo>();

                foreach (DataRow dataRow in TabelaProcesso.Rows)
                {
                    var proc = new Models.ProcessoModel();
                    //proc.id_movimento = int.Parse(dataRow["IDMovimento"].ToString());
                    //proc.data_registro = dataRow["DataRegistro"].ToString();
                    //proc.id_motorista = dataRow["id_motorista"].ToString();
                    //proc.cod_placa = dataRow["cod_placa"].ToString();
                    //proc.numero_cartao = dataRow["NroCartao"].ToString();
                    //proc.nome_transportadora = dataRow["NomeTransportadora"].ToString();
                    //proc.cnpj_transportadora = dataRow["CNPJTransportadora"].ToString();
                    //proc.ecc_cod_transportadora = dataRow["eccCodTransportadora"].ToString();
                    //proc.cod_movimento_tipo = dataRow["CodMovimentoTipo"].ToString();
                    //proc.cod_tipo_origem = dataRow["CodTipoOrigem"].ToString();
                    //proc.observacao = dataRow["Observacao"].ToString();
                    //proc.idc_balanca = dataRow["idc_balanca"].ToString();
                    //proc.peso_balanca_gg = dataRow["PesoBalancaGG"].ToString();
                    //proc.cod_movimento_status = dataRow["CodMovimentoStatus"].ToString();
                    //proc.cria_novo_mov_automatico = dataRow["CriaNovoMovAutomatico"].ToString();
                    //proc.bruto_gscs = dataRow["Bruto_GSCS"].ToString();
                    //proc.tara_gscs = dataRow["Tara_GSCS"].ToString();
                    //proc.per_tolerancia_min_peso = dataRow["per_tolerancia_min_peso"].ToString();
                    //proc.per_tolerancia_max_peso = dataRow["per_tolerancia_max_peso"].ToString();
                    //proc.divergencia_peso = dataRow["Divergencia_Peso"].ToString();
                    //proc.diverg_peso_tara_pendente = dataRow["DivergPeso_TaraPendente"].ToString();
                    //proc.diverge_peso_dth_liberacao = dataRow["DivergPeso_DthLiberacao"].ToString();
                    //proc.diverg_peso_id_resp_liberacao = dataRow["DivergPeso_IdRespLiberacao"].ToString();
                    //proc.diverge_peso_justifica_liberacao = dataRow["DivergPeso_JustificaLiberacao"].ToString();
                    //proc.diverg_peso_ultrapassou = dataRow["DivergPeso_Ultrapassou"].ToString();
                    //proc.id_mov_entrega = dataRow["IDMovEntrega"].ToString();
                    //proc.email_divergencia = dataRow["EmailDivergencia"].ToString();
                    //proc.ignorar_agendamento = dataRow["ignorar_agendamento"].ToString();

                    proc.IdProcesso = int.Parse(dataRow["IdProcesso"].ToString());
                    //proc.Centro = dataRow["Centro"].ToString();
                    //proc.NroCartao = dataRow["NroCartao"].ToString();
                    //proc.CodVeiculo = dataRow["CodVeiculo"].ToString();
                    //proc.Motorista = dataRow["Motorista"].ToString();
                    //proc.Transportadora = dataRow["Transportadora"].ToString();
                    //proc.NroDocumento = dataRow["NroDocumento"].ToString();
                    //proc.TipoDocto = dataRow["TipoDocto"].ToString();
                    //proc.Destinatario = dataRow["Destinatario"].ToString();
                    //proc.TipoOperacao = dataRow["TipoOperacao"].ToString();
                    proc.ProcessoStatus = dataRow["CodProcessoStatus"].ToString();
                    //proc.DataVigenciaInicial = dataRow["DataVigenciaInicial"].ToString();
                    //proc.DataVigenciaFinal = dataRow["DataVigenciaFinal"].ToString();
                    //proc.PesoTara = dataRow["PesoTara"].ToString();
                    //proc.PesoBruto = dataRow["PesoBruto"].ToString();
                    //proc.TipoOrigem = dataRow["TipoOrigem"].ToString();
                    //proc.DataRegistro = dataRow["DataRegistro"].ToString();
                    var procs = new Models.Processo()
                    {
                        Movement = proc
                    };
                    listMovimento.Add(procs);
                }
                var listMovimentos = new Models.Processos
                {
                    Movements = listMovimento
                };
                return listMovimentos;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<Mensagem> UpdateProcesso(int id, string banco, string status)
        {
            SqlCommand cmd = new SqlCommand();
            Connection cn = new Connection();
            var msg = new Mensagem();

            try
            {
                string sqlQuery = "UPDATE PROCESSO SET CodProcessoStatus =" + status + " WHERE IdProcesso = '" + id + "' ";
                cmd.CommandText = sqlQuery;
                cmd.Connection = cn.Conectar(banco);
                cmd.ExecuteNonQuery();
                cn.Desconectar();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable TabelaProcesso = new DataTable();
                da.Fill(TabelaProcesso);

                if (TabelaProcesso.Rows.Count == 0)
                {
                    msg.Msg = "Processo nao encontrado";
                }

                else if (string.IsNullOrEmpty(status))

                {
                    msg.Msg =  "Status inválido ou nulo";
                }

                else
                {
                    string sqlQuery2 = "UPDATE PROCESSO SET CodProcessoStatus ='" + status + "' WHERE IdProcesso = '" + id + "' ";
                    cmd.CommandText = sqlQuery;
                    cmd.Connection = cn.Conectar(banco);
                    cmd.ExecuteReader();
                    cn.Desconectar();
                    SqlDataAdapter da2 = new SqlDataAdapter(cmd);
                    DataTable TabelaProcesso2 = new DataTable();
                    da2.Fill(TabelaProcesso);

                    msg.Msg =  "Processo atualizado com sucesso";
                }
                return msg;
            }
            catch (Exception ex)
            {
                msg.Msg = "Erro";
                return msg;
            }

        }
    }

}







