﻿using APISUPORTE.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APISUPORTE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UnidadesController : Controller
    {
        Configuracao ini = new Configuracao(@"C:\Repositorio\APISuporteBitBucket\apisuporte\APISUPORTE\Config.ini");
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Models.Processos))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult BuscaUnidade()
        {
            int servidores = int.Parse(ini.IniReadValue("UNIDADES", "NUMSERVERS"));
            string[] unidades= new string[servidores+1];
            for (var i = 0; i <= servidores; i++)
            {
                string server = "SERVER" + i.ToString();
                unidades[i] = ini.IniReadValue("UNIDADES", server);
            }
         
            return Ok(unidades);      
        }
    }
    
}
